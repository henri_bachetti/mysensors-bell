EESchema Schematic File Version 2
LIBS:mysensors-bell-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-power-supplies
LIBS:my-microcontrollers
LIBS:my-misc-modules
LIBS:my-regulators
LIBS:my-relays
LIBS:mysensors-bell-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 59907F81
P 8350 4350
F 0 "#PWR01" H 8350 4100 50  0001 C CNN
F 1 "GND" H 8350 4200 50  0000 C CNN
F 2 "" H 8350 4350 50  0000 C CNN
F 3 "" H 8350 4350 50  0000 C CNN
	1    8350 4350
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR02
U 1 1 59907FD5
P 8350 3050
F 0 "#PWR02" H 8350 2900 50  0001 C CNN
F 1 "+3V3" H 8350 3190 50  0000 C CNN
F 2 "" H 8350 3050 50  0000 C CNN
F 3 "" H 8350 3050 50  0000 C CNN
	1    8350 3050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 59908427
P 4750 4850
F 0 "#PWR03" H 4750 4600 50  0001 C CNN
F 1 "GND" H 4750 4700 50  0000 C CNN
F 2 "" H 4750 4850 50  0000 C CNN
F 3 "" H 4750 4850 50  0000 C CNN
	1    4750 4850
	1    0    0    -1  
$EndComp
$Comp
L arduino_mini U1
U 1 1 5992F159
P 6100 4100
F 0 "U1" H 6125 4650 70  0000 C CNN
F 1 "arduino_pro_mini" H 6125 4400 70  0000 C CNN
F 2 "myMicroControllers:Arduino-mini" H 6100 4000 60  0001 C CNN
F 3 "" H 6425 3600 60  0000 C CNN
	1    6100 4100
	-1   0    0    1   
$EndComp
$Comp
L C C1
U 1 1 599178AC
P 4550 4550
F 0 "C1" H 4575 4650 50  0000 L CNN
F 1 "100nF" H 4575 4450 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 4588 4400 50  0001 C CNN
F 3 "" H 4550 4550 50  0000 C CNN
	1    4550 4550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AD34913
P 3900 3400
F 0 "#PWR04" H 3900 3150 50  0001 C CNN
F 1 "GND" H 3900 3250 50  0000 C CNN
F 2 "" H 3900 3400 50  0000 C CNN
F 3 "" H 3900 3400 50  0000 C CNN
	1    3900 3400
	1    0    0    -1  
$EndComp
$Comp
L DFPLAYER U5
U 1 1 5AD34F18
P 8550 6250
F 0 "U5" H 8450 6650 60  0000 C CNN
F 1 "DFPLAYER" H 8450 6450 60  0000 C CNN
F 2 "myModules:DFPLAYER" H 8750 5650 60  0001 C CNN
F 3 "" H 8750 5650 60  0000 C CNN
	1    8550 6250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P5
U 1 1 5AD3564B
P 3500 7250
F 0 "P5" H 3500 7400 50  0000 C CNN
F 1 "AMP-INPUT" V 3600 7250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 3500 7250 50  0001 C CNN
F 3 "" H 3500 7250 50  0000 C CNN
	1    3500 7250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5AD358BB
P 7700 7050
F 0 "#PWR05" H 7700 6800 50  0001 C CNN
F 1 "GND" H 7700 6900 50  0000 C CNN
F 2 "" H 7700 7050 50  0000 C CNN
F 3 "" H 7700 7050 50  0000 C CNN
	1    7700 7050
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5AD35A7E
P 4300 4300
F 0 "R1" V 4380 4300 50  0000 C CNN
F 1 "10K" V 4300 4300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4230 4300 50  0001 C CNN
F 3 "" H 4300 4300 50  0000 C CNN
	1    4300 4300
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR06
U 1 1 5AD35B69
P 4300 4550
F 0 "#PWR06" H 4300 4300 50  0001 C CNN
F 1 "GND" H 4300 4400 50  0000 C CNN
F 2 "" H 4300 4550 50  0000 C CNN
F 3 "" H 4300 4550 50  0000 C CNN
	1    4300 4550
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 5AD35E2B
P 3500 2650
F 0 "P3" H 3500 2800 50  0000 C CNN
F 1 "BELL-BUTTON" V 3600 2600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 3500 2650 50  0001 C CNN
F 3 "" H 3500 2650 50  0000 C CNN
	1    3500 2650
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5AD35F3A
P 3800 2800
F 0 "#PWR07" H 3800 2550 50  0001 C CNN
F 1 "GND" H 3800 2650 50  0000 C CNN
F 2 "" H 3800 2800 50  0000 C CNN
F 3 "" H 3800 2800 50  0000 C CNN
	1    3800 2800
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR08
U 1 1 5AD363C5
P 4750 4200
F 0 "#PWR08" H 4750 4050 50  0001 C CNN
F 1 "+3V3" H 4750 4340 50  0000 C CNN
F 2 "" H 4750 4200 50  0000 C CNN
F 3 "" H 4750 4200 50  0000 C CNN
	1    4750 4200
	1    0    0    -1  
$EndComp
$Comp
L POT RV1
U 1 1 5B0AC725
P 7300 7200
F 0 "RV1" H 7300 7120 50  0000 C CNN
F 1 "VOLUME-47K" V 7450 7550 50  0000 C CNN
F 2 "myPassives:Potentiometer_Omeg_PC20BU" H 7300 7200 50  0001 C CNN
F 3 "" H 7300 7200 50  0000 C CNN
	1    7300 7200
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR09
U 1 1 5B0ACB68
P 7300 7550
F 0 "#PWR09" H 7300 7300 50  0001 C CNN
F 1 "GND" H 7300 7400 50  0000 C CNN
F 2 "" H 7300 7550 50  0000 C CNN
F 3 "" H 7300 7550 50  0000 C CNN
	1    7300 7550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5B0AD036
P 3800 7400
F 0 "#PWR010" H 3800 7150 50  0001 C CNN
F 1 "GND" H 3800 7250 50  0000 C CNN
F 2 "" H 3800 7400 50  0000 C CNN
F 3 "" H 3800 7400 50  0000 C CNN
	1    3800 7400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5B6450D1
P 8250 9350
F 0 "#PWR011" H 8250 9100 50  0001 C CNN
F 1 "GND" H 8250 9200 50  0000 C CNN
F 2 "" H 8250 9350 50  0000 C CNN
F 3 "" H 8250 9350 50  0000 C CNN
	1    8250 9350
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR012
U 1 1 5B6452C9
P 8250 8650
F 0 "#PWR012" H 8250 8500 50  0001 C CNN
F 1 "+3V3" H 8250 8790 50  0000 C CNN
F 2 "" H 8250 8650 50  0000 C CNN
F 3 "" H 8250 8650 50  0000 C CNN
	1    8250 8650
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W1
U 1 1 5B646532
P 4000 5300
F 0 "W1" H 4000 5570 50  0000 C CNN
F 1 "GND" H 4000 5500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 4200 5300 50  0001 C CNN
F 3 "" H 4200 5300 50  0000 C CNN
	1    4000 5300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5CE3B91F
P 7100 6050
F 0 "#PWR013" H 7100 5800 50  0001 C CNN
F 1 "GND" H 7100 5900 50  0000 C CNN
F 2 "" H 7100 6050 50  0000 C CNN
F 3 "" H 7100 6050 50  0000 C CNN
	1    7100 6050
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5CE3BB64
P 7100 5800
F 0 "C4" H 7125 5900 50  0000 L CNN
F 1 "1µF MLCC" H 6950 5700 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 7138 5650 50  0001 C CNN
F 3 "" H 7100 5800 50  0000 C CNN
	1    7100 5800
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5CE3E261
P 7800 3400
F 0 "C5" H 7700 3300 50  0000 L CNN
F 1 "100nF" H 7850 3500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 7838 3250 50  0001 C CNN
F 3 "" H 7800 3400 50  0000 C CNN
	1    7800 3400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 5CE3E3B2
P 7800 3850
F 0 "#PWR014" H 7800 3600 50  0001 C CNN
F 1 "GND" H 7800 3700 50  0000 C CNN
F 2 "" H 7800 3850 50  0000 C CNN
F 3 "" H 7800 3850 50  0000 C CNN
	1    7800 3850
	1    0    0    -1  
$EndComp
$Comp
L NRF24L01 U4
U 1 1 5D1CE8E9
P 8350 3700
F 0 "U4" H 8200 3950 60  0000 C CNN
F 1 "NRF24L01" H 8100 4100 60  0000 C CNN
F 2 "myModules:NRF24L01" H 8350 3300 60  0001 C CNN
F 3 "" H 8350 3300 60  0000 C CNN
	1    8350 3700
	-1   0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5D7F4B3B
P 7350 5200
F 0 "R3" V 7430 5200 50  0000 C CNN
F 1 "1K" V 7350 5200 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7280 5200 50  0001 C CNN
F 3 "" H 7350 5200 50  0000 C CNN
	1    7350 5200
	-1   0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5D7F4E58
P 7550 5200
F 0 "R4" V 7630 5200 50  0000 C CNN
F 1 "1K" V 7550 5200 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7480 5200 50  0001 C CNN
F 3 "" H 7550 5200 50  0000 C CNN
	1    7550 5200
	-1   0    0    -1  
$EndComp
$Comp
L CP C6
U 1 1 5D7F644A
P 4000 4300
F 0 "C6" H 4025 4400 50  0000 L CNN
F 1 "220µF 6.3V" H 3750 4200 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D6.3_L11.2_P2.5" H 4038 4150 50  0001 C CNN
F 3 "" H 4000 4300 50  0000 C CNN
	1    4000 4300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5D7F6550
P 4000 4550
F 0 "#PWR015" H 4000 4300 50  0001 C CNN
F 1 "GND" H 4000 4400 50  0000 C CNN
F 2 "" H 4000 4550 50  0000 C CNN
F 3 "" H 4000 4550 50  0000 C CNN
	1    4000 4550
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR016
U 1 1 5D7F7282
P 4000 3000
F 0 "#PWR016" H 4000 2850 50  0001 C CNN
F 1 "+3V3" H 4000 3140 50  0000 C CNN
F 2 "" H 4000 3000 50  0000 C CNN
F 3 "" H 4000 3000 50  0000 C CNN
	1    4000 3000
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P9
U 1 1 5D80E361
P 3500 6600
F 0 "P9" H 3500 6750 50  0000 C CNN
F 1 "PLAYER-OUT" V 3600 6600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 3500 6600 50  0001 C CNN
F 3 "" H 3500 6600 50  0000 C CNN
	1    3500 6600
	-1   0    0    -1  
$EndComp
$Comp
L HEATSINK HS1
U 1 1 5D80FA5C
P 6450 8450
F 0 "HS1" H 6450 8650 50  0000 C CNN
F 1 "HEATSINK" H 6450 8400 50  0000 C CNN
F 2 "myHeatSinks:HEAT-SINK-TO220-24x12x30" H 6450 8450 50  0001 C CNN
F 3 "" H 6450 8450 50  0000 C CNN
	1    6450 8450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 5D81059B
P 7050 8000
F 0 "#PWR017" H 7050 7850 50  0001 C CNN
F 1 "+5V" H 7050 8140 50  0000 C CNN
F 2 "" H 7050 8000 50  0000 C CNN
F 3 "" H 7050 8000 50  0000 C CNN
	1    7050 8000
	1    0    0    -1  
$EndComp
$Comp
L LM7805CT U2
U 1 1 5D81E979
P 6450 8800
F 0 "U2" H 6250 9000 50  0000 C CNN
F 1 "LM7805CT" H 6450 9000 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 6450 8900 50  0001 C CIN
F 3 "" H 6450 8800 50  0000 C CNN
	1    6450 8800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P10
U 1 1 5D820EEF
P 3500 2250
F 0 "P10" H 3500 2400 50  0000 C CNN
F 1 "SELECT" V 3600 2250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 3500 2250 50  0001 C CNN
F 3 "" H 3500 2250 50  0000 C CNN
	1    3500 2250
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 5D84C5B2
P 3500 5350
F 0 "P2" H 3500 5500 50  0000 C CNN
F 1 "MAIN-POWER" V 3600 5350 50  0000 C CNN
F 2 "myConnectors:NINIGI_NS25-W2P" H 3500 5350 50  0001 C CNN
F 3 "" H 3500 5350 50  0000 C CNN
	1    3500 5350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 5D84C933
P 3800 5500
F 0 "#PWR018" H 3800 5250 50  0001 C CNN
F 1 "GND" H 3800 5350 50  0000 C CNN
F 2 "" H 3800 5500 50  0000 C CNN
F 3 "" H 3800 5500 50  0000 C CNN
	1    3800 5500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 5D84D1B3
P 9050 5000
F 0 "#PWR019" H 9050 4750 50  0001 C CNN
F 1 "GND" H 9050 4850 50  0000 C CNN
F 2 "" H 9050 5000 50  0000 C CNN
F 3 "" H 9050 5000 50  0000 C CNN
	1    9050 5000
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P4
U 1 1 5D84DA71
P 3500 3250
F 0 "P4" H 3500 3500 50  0000 C CNN
F 1 "LDR-PIR" V 3600 3250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3500 3250 50  0001 C CNN
F 3 "" H 3500 3250 50  0000 C CNN
	1    3500 3250
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X03 P6
U 1 1 5D84E122
P 9350 4800
F 0 "P6" H 9350 5000 50  0000 C CNN
F 1 "RELAYS" V 9450 4800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 9350 4800 50  0001 C CNN
F 3 "" H 9350 4800 50  0000 C CNN
	1    9350 4800
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR020
U 1 1 5D85010F
P 3800 5200
F 0 "#PWR020" H 3800 5050 50  0001 C CNN
F 1 "VDD" H 3800 5350 50  0000 C CNN
F 2 "" H 3800 5200 50  0000 C CNN
F 3 "" H 3800 5200 50  0000 C CNN
	1    3800 5200
	1    0    0    -1  
$EndComp
$Comp
L HT7533-1 U3
U 1 1 5D851084
P 7650 8800
F 0 "U3" H 7800 8750 50  0000 C CNN
F 1 "HT7533-1" H 7650 9000 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 7650 8900 50  0001 C CIN
F 3 "" H 7650 8800 50  0000 C CNN
	1    7650 8800
	1    0    0    -1  
$EndComp
Text Notes 4650 7750 0    60   ~ 0
VDD : 5V to 24V
Text Notes 4750 7350 0    60   ~ 0
P5 and RV1 : if an amplifier is used
Text Notes 4750 6500 0    60   ~ 0
P9 : if no amplifier is used
$Comp
L CP C7
U 1 1 5D858C2A
P 8250 9000
F 0 "C7" H 8275 9100 50  0000 L CNN
F 1 "10µF 10V" H 8050 8900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2" H 8288 8850 50  0001 C CNN
F 3 "" H 8250 9000 50  0000 C CNN
	1    8250 9000
	1    0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 5D858C88
P 7050 9000
F 0 "C3" H 7075 9100 50  0000 L CNN
F 1 "10µF 10V" H 6850 8900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2" H 7088 8850 50  0001 C CNN
F 3 "" H 7050 9000 50  0000 C CNN
	1    7050 9000
	1    0    0    -1  
$EndComp
Text Notes 7600 4650 0    60   ~ 0
U4 & C5 : for domotic server
$Comp
L +5V #PWR021
U 1 1 5D8880B0
P 7750 5450
F 0 "#PWR021" H 7750 5300 50  0001 C CNN
F 1 "+5V" H 7750 5590 50  0000 C CNN
F 2 "" H 7750 5450 50  0000 C CNN
F 3 "" H 7750 5450 50  0000 C CNN
	1    7750 5450
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5D888ABE
P 5850 9000
F 0 "C2" H 5875 9100 50  0000 L CNN
F 1 "470nF" H 5875 8900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 5888 8850 50  0001 C CNN
F 3 "" H 5850 9000 50  0000 C CNN
	1    5850 9000
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR022
U 1 1 5D888DE6
P 5850 8000
F 0 "#PWR022" H 5850 7850 50  0001 C CNN
F 1 "VDD" H 5850 8150 50  0000 C CNN
F 2 "" H 5850 8000 50  0000 C CNN
F 3 "" H 5850 8000 50  0000 C CNN
	1    5850 8000
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NO_Small JP1
U 1 1 5D889B9D
P 6450 8100
F 0 "JP1" H 6450 8180 50  0000 C CNN
F 1 "PWR-SOURCE" H 6460 8040 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6450 8100 50  0001 C CNN
F 3 "" H 6450 8100 50  0000 C CNN
	1    6450 8100
	1    0    0    -1  
$EndComp
Text Notes 4650 8150 0    60   ~ 0
JP1 closed if VDD = 5V
Connection ~ 7050 8100
Wire Wire Line
	7050 8100 6550 8100
Connection ~ 5850 8100
Wire Wire Line
	5850 8100 6350 8100
Wire Wire Line
	5850 9150 5850 9250
Connection ~ 5850 8750
Wire Wire Line
	5850 8000 5850 8850
Connection ~ 7650 9250
Wire Wire Line
	7050 9250 7050 9150
Connection ~ 7050 8750
Wire Wire Line
	9050 4900 9150 4900
Wire Wire Line
	3800 3400 3700 3400
Wire Wire Line
	3800 3950 3800 3400
Wire Wire Line
	4000 3300 3700 3300
Wire Wire Line
	3900 3200 3700 3200
Wire Wire Line
	9050 4900 9050 5000
Connection ~ 3800 5400
Wire Wire Line
	4000 5400 4000 5300
Wire Wire Line
	3700 5400 4000 5400
Wire Wire Line
	3800 5500 3800 5400
Wire Wire Line
	3800 5300 3800 5200
Wire Wire Line
	3700 5300 3800 5300
Wire Wire Line
	7300 2200 3700 2200
Wire Wire Line
	7300 4250 7300 2200
Wire Wire Line
	7150 4250 7300 4250
Connection ~ 3800 2700
Wire Wire Line
	3700 2300 3800 2300
Connection ~ 6450 9250
Wire Wire Line
	5850 8750 6050 8750
Wire Wire Line
	6450 9250 6450 9050
Connection ~ 7550 6950
Wire Wire Line
	7550 6650 7550 6950
Wire Wire Line
	3700 6650 7550 6650
Wire Wire Line
	3700 6550 7850 6550
Wire Wire Line
	4750 3800 5100 3800
Connection ~ 4000 3950
Wire Wire Line
	4000 4150 4000 3950
Wire Wire Line
	4000 4450 4000 4550
Wire Wire Line
	7550 5350 7550 5750
Wire Wire Line
	7550 5050 7550 3650
Wire Wire Line
	7350 3800 7350 5050
Wire Wire Line
	7350 5350 7350 5950
Connection ~ 8350 3150
Wire Wire Line
	8350 3150 7800 3150
Wire Wire Line
	8350 3050 8350 3150
Wire Wire Line
	8350 4350 8350 4250
Wire Wire Line
	4800 3100 3700 3100
Connection ~ 4750 4800
Wire Wire Line
	4550 4800 4550 4700
Wire Wire Line
	4750 4800 4550 4800
Wire Wire Line
	4750 4200 4750 4400
Wire Wire Line
	4750 4700 4750 4850
Wire Wire Line
	4750 4700 5100 4700
Wire Wire Line
	4750 4400 5100 4400
Wire Wire Line
	8900 2750 8900 3800
Wire Wire Line
	8950 3700 8800 3700
Wire Wire Line
	7600 3350 7150 3350
Wire Wire Line
	7600 3350 7600 3600
Wire Wire Line
	7550 3500 7150 3500
Wire Wire Line
	7550 2800 7550 3500
Wire Wire Line
	9000 2800 7550 2800
Wire Wire Line
	9000 3600 9000 2800
Wire Wire Line
	7800 3150 7800 3250
Wire Wire Line
	8900 2750 4950 2750
Wire Wire Line
	4950 2750 4950 3500
Wire Wire Line
	4950 3500 5100 3500
Wire Wire Line
	7650 3700 7650 2700
Wire Wire Line
	7650 2700 4900 2700
Wire Wire Line
	4900 2700 4900 3350
Wire Wire Line
	4900 3350 5100 3350
Wire Wire Line
	8950 3700 8950 2650
Wire Wire Line
	8950 2650 4850 2650
Wire Wire Line
	4850 2650 4850 3650
Wire Wire Line
	4850 3650 5100 3650
Connection ~ 4750 4300
Wire Wire Line
	4550 4300 4750 4300
Wire Wire Line
	4550 4400 4550 4300
Wire Wire Line
	3900 3400 3900 3200
Wire Wire Line
	4000 3000 4000 3300
Wire Wire Line
	7750 5450 7750 5550
Wire Wire Line
	7100 5550 7850 5550
Wire Wire Line
	7550 5750 7850 5750
Wire Wire Line
	7350 5950 7850 5950
Wire Wire Line
	7550 3650 7150 3650
Wire Wire Line
	7300 6950 7850 6950
Wire Wire Line
	7700 7050 7700 6750
Wire Wire Line
	7700 6750 7850 6750
Wire Wire Line
	4300 4550 4300 4450
Wire Wire Line
	4300 3950 4300 4150
Connection ~ 4300 3950
Wire Wire Line
	3700 2700 3800 2700
Wire Wire Line
	3800 2300 3800 2800
Wire Wire Line
	3700 2600 7250 2600
Wire Wire Line
	7250 2600 7250 4100
Wire Wire Line
	7500 3200 7500 4700
Wire Wire Line
	7150 3800 7350 3800
Wire Wire Line
	7500 3200 7150 3200
Wire Wire Line
	7300 7350 7300 7550
Wire Wire Line
	7300 6950 7300 7050
Wire Wire Line
	3700 7200 7150 7200
Wire Wire Line
	3800 7400 3800 7300
Wire Wire Line
	3800 7300 3700 7300
Wire Wire Line
	6850 8750 7250 8750
Wire Wire Line
	8250 9150 8250 9350
Wire Wire Line
	7650 9250 7650 9050
Connection ~ 8250 9250
Wire Wire Line
	8250 8650 8250 8850
Wire Wire Line
	8250 8750 8050 8750
Connection ~ 8250 8750
Wire Wire Line
	4800 3100 4800 2550
Wire Wire Line
	4800 2550 7450 2550
Wire Wire Line
	7450 2550 7450 3950
Wire Wire Line
	7400 2500 7400 4800
Wire Wire Line
	7400 2500 4750 2500
Wire Wire Line
	4750 2500 4750 3800
Wire Wire Line
	7250 4100 7150 4100
Wire Wire Line
	7450 3950 7150 3950
Wire Wire Line
	7100 6050 7100 5950
Wire Wire Line
	7100 5650 7100 5550
Connection ~ 7750 5550
Wire Wire Line
	7800 3550 7800 3850
Wire Wire Line
	3800 3950 5100 3950
Wire Wire Line
	7400 4800 9150 4800
Wire Wire Line
	7500 4700 9150 4700
Wire Wire Line
	7050 8000 7050 8850
Connection ~ 7050 9250
Wire Wire Line
	8900 3800 8800 3800
Wire Wire Line
	9000 3600 8800 3600
Wire Wire Line
	7900 3700 7650 3700
Wire Wire Line
	7600 3600 7900 3600
Wire Wire Line
	5850 9250 8250 9250
$EndSCHEMATC
