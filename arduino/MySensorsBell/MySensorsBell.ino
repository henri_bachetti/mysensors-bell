
/*
   SD CARD is organized as shown below:
   FOLDER 01
      001-cuckoo.mp3
      002-finch.mp3
      003-raven.mp3
      004-crow.mp3
      005-magpie.mp3
   FOLDER 02
      001-dog.mp3
      002-crocodile.mp3
      003-aligator.mp3
      004-gorilla.mp3
*/

#include <SoftwareSerial.h>
#include <DFRobotDFPlayerMini.h>

#define USE_MYSENSORS

#ifdef USE_MYSENSORS
//#define MY_DEBUG
// Enable and select radio type attached
#define MY_RADIO_RF24
//#define MY_RADIO_RFM69
#define MY_RF24_CE_PIN    7
#define MY_RF24_CS_PIN    8
#include <MySensors.h>
#endif

#include <Bounce2.h>

#define BUTTON_CHILD_ID   1
#define PIR_CHILD_ID      2
#define LDR_CHILD_ID      3
#define RING_CHILD_ID     4
#define SOUND_CHILD_ID    5

#define EEPROM_RING       1
#define EEPROM_SOUND      2

#define SELECT_PIN        2
#define BUTTON_PIN        3
#define PIR_PIN           4
#define AMP_POWER_PIN     A0
#define LIGHT_PIN         9
#define LUMINOSITY_PIN    1

#define FOLDER_RING       1
#define FOLDER_SOUNDS     2

// change these values if necessary
#define NIGHT_LUMINOSOTY  5
#define DIMMER
#define LIGHT_DURATION    60L

SoftwareSerial playerSoftSerial(5, 6); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
Bounce selectDebouncer = Bounce();
Bounce buttonDebouncer = Bounce();

#ifdef USE_MYSENSORS
// Change to V_LIGHT if you use S_LIGHT in presentation below
MyMessage msg(BUTTON_CHILD_ID, V_TRIPPED);
MyMessage luminosityMsg(LDR_CHILD_ID, V_LIGHT_LEVEL);
MyMessage soundMsg(SOUND_CHILD_ID, V_STATUS);
#endif

void printDetail(uint8_t type, int value);

uint8_t presenceSound = 1;
uint8_t ringSound = 1;
uint8_t playing;

int readPir(void)
{
  static int pirState = LOW;
  int val;

  val = digitalRead(PIR_PIN);  // read input value
  if (val == HIGH) { // check if the input is HIGH
    delay(150);
    if (pirState == LOW) {
      Serial.println(F("Motion detected!"));
      pirState = HIGH;
    }
    return true;
  }
  else {
    delay(300);
    if (pirState == HIGH) {
      Serial.println(F("Motion ended!"));
      pirState = LOW;
    }
  }
  return false;
}

#define VREF 5.0
#define RREF 10000.0
#define LUX_CALC_SCALAR 14500000
#define LUX_CALC_EXPONENT -1.405
#define NSAMPLE 10

int readLdr(void)
{
  uint32_t adc = 0;

  for (int i = 0 ; i < NSAMPLE ; i++) {
    adc += analogRead(LUMINOSITY_PIN);
    delay(10);
  }
  adc /= NSAMPLE;
  float voltage = VREF - (adc * VREF / 1023);
  float current = (float)adc * VREF / 1023 / RREF;
  float resistance = voltage / current;
  return (LUX_CALC_SCALAR * pow(resistance, LUX_CALC_EXPONENT));
}

int getLuminosity(void)
{
  static int actualLux = -1;
  int lux = readLdr();
  if (actualLux != lux) {
#ifdef USE_MYSENSORS
    send(luminosityMsg.set(lux, 2));
#endif
    Serial.print(F("LUX: ")); Serial.println(lux);
    actualLux = lux;
  }
  return lux < NIGHT_LUMINOSOTY ? true : false;
}

void play(int folder, int file)
{
  if (playing == 0) {
    digitalWrite(AMP_POWER_PIN, HIGH);
    delay(500);
    Serial.print(F("Play the ")); Serial.print(folder); Serial.print(F(", ")); Serial.println(file);
    myDFPlayer.playFolder(folder, file);
    playing = folder;
  }
}

void powerOffSound(void)
{
  digitalWrite(AMP_POWER_PIN, LOW);
}

void light(int state)
{
  static int isOn;
  static int pwm;

  if (state != isOn) {
    Serial.print(F("Turn the light ")); Serial.println(state ? "ON" : "OFF");
    isOn = state;
  }
#ifdef DIMMER
  if (state) {
    while (pwm < 255) {
      analogWrite(LIGHT_PIN, pwm++);
      delay(15);
    }
  }
  else {
    while (pwm != 0) {
      pwm--;
      analogWrite(LIGHT_PIN, pwm);
      delay(10);
    }
  }
#else
  digitalWrite(LIGHT_PIN, state ? HIGH : LOW);
#endif
}

void checkSelectionButton()
{
  static int oldSelectState = -1;

  selectDebouncer.update();
  int selectPressed = !selectDebouncer.read();
  if (selectPressed != oldSelectState) {
    if (selectPressed) {
      Serial.println(F("SELECT pressed"));
      if (playing == FOLDER_RING) {
        ringSound++;
        checkRingSound(true);
      }
      else {
        presenceSound++;
        checkPresenceSound(true);
      }
    }
    oldSelectState = selectPressed;
  }
}

void checkBellButton()
{
  static int oldButtonState = -1;

  buttonDebouncer.update();
  int buttonPressed = !buttonDebouncer.read();
  if (buttonPressed != oldButtonState) {
#ifdef USE_MYSENSORS
    send(msg.setSensor(BUTTON_CHILD_ID).set(buttonPressed == HIGH ? 1 : 0));
#endif
    oldButtonState = buttonPressed;
    if (buttonPressed) {
      Serial.println(F("BUTTON pressed"));
      bell();
    }
  }
}

void bell(void)
{
  int nfile = myDFPlayer.readFileCountsInFolder(FOLDER_RING);
  Serial.print(nfile); Serial.println(F(" files in the RING folder"));
  if (ringSound > nfile) {
    ringSound = 1;
  }
  playing = 0;
  play(FOLDER_RING, ringSound);
}

void checkRingSound(bool playIt)
{
  myDFPlayer.stop();
  int nfile = myDFPlayer.readFileCountsInFolder(FOLDER_RING);
  Serial.print(nfile); Serial.println(F(" files in the RING folder"));
  if (ringSound > nfile) {
    ringSound = 1;
  }
  saveState(EEPROM_RING, ringSound);
  if (playIt) {
    playing = 0;
    play(FOLDER_RING, ringSound);
  }
}

void checkPresenceSound(bool playIt)
{
  myDFPlayer.stop();
  int nfile = myDFPlayer.readFileCountsInFolder(FOLDER_SOUNDS);
  Serial.print(nfile); Serial.println(F(" files in the SOUNDS folder"));
  if (presenceSound > nfile) {
    presenceSound = 1;
  }
  saveState(EEPROM_SOUND, presenceSound);
  if (playIt) {
    playing = 0;
    play(FOLDER_SOUNDS, presenceSound);
  }
}

void setup()
{
  Serial.begin(115200);
  pinMode(SELECT_PIN, INPUT_PULLUP);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  selectDebouncer.attach(SELECT_PIN);
  selectDebouncer.interval(5);
  buttonDebouncer.attach(BUTTON_PIN);
  buttonDebouncer.interval(5);
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(AMP_POWER_PIN, OUTPUT);
  digitalWrite(LIGHT_PIN, LOW);
  pinMode (PIR_PIN, INPUT);
  playerSoftSerial.begin(9600);

  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(playerSoftSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while (true) {
      delay(0); // Code to be compatible with ESP8266 watch dog.
    }
  }
  Serial.println(F("DFPlayer Mini online."));
  myDFPlayer.volume(30);  //Set volume value. From 0 to 30
  ringSound = loadState(EEPROM_RING);
  checkRingSound(false);
  presenceSound = loadState(EEPROM_SOUND);
  checkPresenceSound(false);
}

#ifdef USE_MYSENSORS
void presentation()
{
  present(BUTTON_CHILD_ID, S_DOOR);
  present(PIR_CHILD_ID, S_MOTION);
  present(LDR_CHILD_ID, S_LIGHT_LEVEL);
  present(RING_CHILD_ID, S_BINARY);
  present(SOUND_CHILD_ID, S_BINARY);
}
#endif

void loop()
{
  int night;
  static long lightStart;
  static int oldPirState = -1;

  checkSelectionButton();
  checkBellButton();
  if (lightStart) {
    if (lightStart + (LIGHT_DURATION * 1000) < millis()) {
      light(false);
    }
    else {
      light(true);
    }
  }
  night = getLuminosity();
  int presence = readPir();
  if (presence != oldPirState) {
#ifdef USE_MYSENSORS
    send(msg.setSensor(PIR_CHILD_ID).set(presence == HIGH ? 1 : 0));
    Serial.print(F("motion sent to server: ")); Serial.println(presence);
#endif
    if (presence == HIGH) {
      play(FOLDER_SOUNDS, presenceSound);
      if (night) {
        lightStart = millis();
        Serial.print(F("lightStart: ")); Serial.println(lightStart);
      }
    }
    oldPirState = presence;
  }
  if (myDFPlayer.available()) {
    printDetail(myDFPlayer.readType(), myDFPlayer.read()); //Print the detail message from DFPlayer to handle different errors and states.
  }
}

void printDetail(uint8_t type, int value) {
  switch (type) {
    case TimeOut:
      Serial.println(F("Time Out!"));
      break;
    case WrongStack:
      Serial.println(F("Stack Wrong!"));
      break;
    case DFPlayerCardInserted:
      Serial.println(F("Card Inserted!"));
      break;
    case DFPlayerCardRemoved:
      Serial.println(F("Card Removed!"));
      break;
    case DFPlayerCardOnline:
      Serial.println(F("Card Online!"));
      break;
    case DFPlayerPlayFinished:
      Serial.print(F("Number:"));
      Serial.print(value);
      Serial.println(F(" Play Finished!"));
      playing = 0;
      powerOffSound();
      break;
    case DFPlayerError:
      Serial.print(F("DFPlayerError:"));
      switch (value) {
        case Busy:
          Serial.println(F("Card not found"));
          break;
        case Sleeping:
          Serial.println(F("Sleeping"));
          break;
        case SerialWrongStack:
          Serial.println(F("Get Wrong Stack"));
          break;
        case CheckSumNotMatch:
          Serial.println(F("Check Sum Not Match"));
          break;
        case FileIndexOut:
          Serial.println(F("File Index Out of Bound"));
          break;
        case FileMismatch:
          Serial.println(F("Cannot Find File"));
          break;
        case Advertise:
          Serial.println(F("In Advertise"));
          break;
        default:
          Serial.println(F("????"));
          break;
      }
      break;
    default:
      break;
  }
}

#ifdef USE_MYSENSORS
void receive(const MyMessage &message)
{
  // We only expect one type of message from controller. But we better check anyway.
  if (message.isAck()) {
    Serial.println(F("ACK from gateway"));
  }
  else {
    if (message.type == V_STATUS) {
      // Write some debug info
      Serial.print(F("Incoming change for sensor: ")); Serial.print(message.sensor); Serial.print(F(", New status: ")); Serial.println(message.getBool());
      switch (message.sensor) {
        case BUTTON_CHILD_ID:
          if (message.getBool() == 1) {
            bell();
            send(msg.setSensor(BUTTON_CHILD_ID).set(0));
          }
          break;
        case PIR_CHILD_ID:
          if (message.getBool() == 1) {
            play(FOLDER_SOUNDS, presenceSound);
          }
          break;
        case RING_CHILD_ID:
          if (message.getBool() == 1) {
            ringSound++;
            checkRingSound(true);
            send(msg.setSensor(RING_CHILD_ID).set(0));
          }
          break;
        case SOUND_CHILD_ID:
          if (message.getBool() == 1) {
            presenceSound++;
            checkPresenceSound(true);
            send(msg.setSensor(SOUND_CHILD_ID).set(0));
          }
          break;
      }
    }
  }
}
#endif

