# MYSENSORS MP3 BELL

The purpose of this page is to explain step by step the realization of an MP3 door bell based on ARDUINO PRO MINI. It can be connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO PRO MINI 3.3V 8MHz

 * an NRF24L01
 * a DFPLAYER
 * an HT-7533-1 regulator
 * a power supply
 * some passive components

### ELECTRONICS

The schematics is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/09/un-carillon-mp3.html

